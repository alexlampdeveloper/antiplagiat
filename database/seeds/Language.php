<?php

use Illuminate\Database\Seeder;

class Language extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = ['pascal'];
        foreach($languages as $key => $value){
            if(!DB::table('language')->where('name', $value)->count()){
                DB::table('language')->insert([
                    'name' => $value
                ]);
            }
        }
    }
}
