<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\Deploy;
use App\Console\Commands\Phpunit;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.deploy', function()
        {
            return new Deploy;
        });

        $this->app->singleton('command.phpunit', function()
        {
            return new Phpunit;
        });


        $this->commands(
            'command.deploy',
            'command.phpunit'
        );
    }
}