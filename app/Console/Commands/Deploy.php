<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Deploy extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'deploy';

    protected $signature = 'deploy {--clear} {--phpunit}  {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deploy.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $clear = $this->option('clear') || $this->option('all');
        $phpunit = $this->option('phpunit') || $this->option('all');
        if($clear){
            $this->call('migrate:rollback');
        }
        $this->call('migrate');
        $this->call('db:seed');
        if($phpunit){
            $this->call('phpunit');
        }
    }

}