<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Phpunit extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'phpunit';

    protected $signature = 'phpunit {file?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run phpunit.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->argument('file');
        if($file){
            $this->info(exec('php vendor/phpunit/phpunit/phpunit '.$file));
        }else{
            $queue = ['tests'];
            $files = [];
            while(count($queue)>0){
                $dir = array_shift($queue);
                $scan = scandir($dir);
                foreach ($scan as $key => $value) {
                    if($value == '.' || $value == '..' || $value == 'TestCase.php'){
                        continue; 
                    }
                    if(strpos($value, '.php')){
                        $files[] = $dir.'/'.$value;
                    }else{
                        $queue[] = $dir.''.$value;
                    }
                }
            }
            $bar = $this->output->createProgressBar(count($files));
            foreach ($files as $key => $value) {
                exec('php vendor/phpunit/phpunit/phpunit '.$value, $result[$value]);
                $bar->advance();
            }
            $bar->finish();
            $this->line('');
            $header = ['file', 'output']; 
            foreach ($result as $key => $value) {
                $this->line('_____________________________________________');
                $this->line('');
                $this->info('   '.$key);
                $this->line('_____________________________________________');
                foreach ($value as $key1 => $value1) {
                    if($value1 == 'PHPUnit 6.4.3 by Sebastian Bergmann and contributors.')
                        continue;
                    $this->line($value1);
                }
            }
        }
    }

}