<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postLogin(Request $request)
    {
        $this->jwt->setToken($request->input('token'));
        $token =  $this->jwt->getToken();
        $decoded =  $this->jwt->decode($token);
        $userData = [
            'email' => $decoded->get('email'),
            'password' => $decoded->get('password'),
        ];
        $valide = true;
        if(empty($userData['email']) || strlen($userData['email']) > 255 || !filter_var($userData['email'], FILTER_VALIDATE_EMAIL)){
            $valide = false;
        }
        if(empty($userData['password']) || strlen($userData['password']) < 8){
            $valide = false;
        }
        if($valide == false){
            return response()->json(['error' => 'Invalid email or password'], 400);
        }
        $users = User::where('email', $userData['email'])->get();
        if(count($users) > 0){
            $user = $users[0];
            if(Hash::check($userData['password'], $user->password)){
                $user->token = uniqid();
                $user->save();
                return response()->json(['token' => $user->token], 200);
            }else{
                return response()->json(['error' => 'Incorrect password'], 400);
            }
        }else{
            return response()->json(['error' => 'User doesn`t exist'], 400);
        }
    }

    public function register(Request $request){
        $this->jwt->setToken($request->input('token'));
        $token =  $this->jwt->getToken();
        $decoded =  $this->jwt->decode($token);
        $userData = [
            'first_name' => $decoded->get('first_name'),
            'last_name' => $decoded->get('last_name'),
            'email' => $decoded->get('email'),
            'ip' => $request->ip(),
            'password' => $decoded->get('password'),
        ];
        $valide = true;
        if(empty($userData['first_name'])){
            $valide = false;
        }
        if(empty($userData['last_name'])){
            $valide = false;
        }
        if(empty($userData['email']) || strlen($userData['email']) > 255 || !filter_var($userData['email'], FILTER_VALIDATE_EMAIL)){
            $valide = false;
        }
        if(empty($userData['ip']) || !filter_var($userData['ip'], FILTER_VALIDATE_IP)){
            $valide = false;
        }
        if(empty($userData['password']) || strlen($userData['password']) < 8){
            $valide = false;
        }
        if($valide == false){
            return response()->json(['error' => 'Invalid user data'], 400);
        }
        if(count(User::where('email', $userData['email'])->get()) > 0){
           return response()->json(['error' => 'User email already exist'], 400);   
        }
        $userData['password'] = Hash::make($userData['password']);

        $user = User::create($userData);
        return response(null, 200);
    }
}