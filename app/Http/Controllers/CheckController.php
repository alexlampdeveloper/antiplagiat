<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Code;
use App\Language;

class CheckController extends Controller
{

    protected $jwt;
    protected $user;
    protected $language;
    protected $codes;

    protected $comments;
    protected $vars;
    protected $oprs;

    protected $percentes;

    protected $types = ['integer', 'byte', 'word', 'shortint', 'longint', 'real', 'single', 'double', 'extended', 'comp', 'pointer', 'boolean', 'char', 'string'];

    protected $operators = ['\+', '-', '\*', '\/', '%', '=', '<>', '>', '<', '>=', '<=', 'and', 'and then', 'or', 'or else', 'not', '&', '\|', '!', '~', '<<', '>>', 'xor', 'shl', 'shr'];

    protected $plagiat;
    protected $result;
        
    
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function start(Request $request){
        $this->jwt->setToken($request->input('token'));
        $token =  $this->jwt->getToken();
        $decoded =  $this->jwt->decode($token);
        $user_token = $decoded->get('sub');
        if(empty($token)){
            return response()->json(['error' => 'Token required'], 400);
        }
        $this->user = User::getByToken($user_token);
        if(empty($this->user)){
            return response()->json(['error' => 'Token incorrect'], 400);
        }
        $language = $decoded->get('language');
        if(empty($language)){
            return response()->json(['error' => 'Language required'], 400);
        }
        $this->language = Language::getByName($language);
        if(empty($this->language)){
            return response()->json(['error' => 'Language incorrect'], 400);
        }
        $this->codes = Code::getForUserByLanguage($this->user, $this->language);
        if(count($this->codes)<2){
            return response()->json(['error' => 'Number of codes must be at least 2'], 400);
        }
        //_______COMMENTS_____
        $this->getComments();
        $this->checkComments();
        $this->deleteComments();
        //_______VARS_________
        $this->getVars();
        $this->checkVars();
        //_______SPACES_______
        $this->getOperators();
        $this->checkOperators();
        //_______RESULT_______
        $this->calculatePlagiat();
        $this->getResult();
        $this->reset();
        return response()->json(['result' => json_encode($this->result)], 200);
    }

    private function deleteComments(){
        foreach ($this->codes as $key => $value) {
            $value->code = preg_replace('/\/\/.*/', '', $value->code);
            $value->code = preg_replace('/{.*}/s', '', $value->code);
        }
    }

    private function getComments(){
        foreach ($this->codes as $key => $value) {
            $this->comments[$value->id] = [];
            preg_match_all('/\/\/.*/', $value->code, $matches);
            foreach ($matches as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    $matches[$key1][$key2] = explode(' ', substr($value2, 2));
                    foreach ($matches[$key1][$key2] as $key3 => $value3) {
                        if(!empty($value3)){
                            $this->comments[$value->id][] = $value3;
                        }
                    }
                }
            }
            preg_match_all('/{.*}/s', $value->code, $matches);
            foreach ($matches as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    $matches[$key1][$key2] = explode(' ', substr($value2, 1, -1));
                    foreach ($matches[$key1][$key2] as $key3 => $value3) {
                        if(!empty($value3)){
                            $this->comments[$value->id][] = $value3;
                        }
                    }
                }
            }
            $matches = [];
        }
    }

    private function checkComments(){
        $this->percentes['comments'] = [];
        foreach ($this->comments as $key => $value) {
            foreach ($this->comments as $key1 => $value1) {
                if($key1==$key){
                    continue;
                }
                $count_matches = 0;
                foreach ($value as $key2 => $value2) {
                    foreach ($value1 as $key3 => $value3){
                        if($value2 == $value3){
                            $count_matches+=1;
                            break;
                        }elseif (stripos($value2, $value3) != false || stripos($value3, $value2) != false){
                            $count_matches+=0.5;
                            break;
                        }
                    }
                }
                $this->percentes['comments'][$key][$key1] = (count($value) == 0?0:100 / count($value) * $count_matches);
                $this->percentes['comments'][$key1][$key] = (count($value) == 0?0:100 / count($value1) * $count_matches);
            }
        }
    }

    private function getVars(){
        foreach ($this->codes as $key => $value) {
            foreach ($this->types as $k => $type) {
                $matches = [];
                preg_match_all('/(\S*):'.$type.'/', $value->code, $matches);
                array_shift($matches);
                $this->vars[$value->id][$type] = [];
                foreach ($matches as $key1 => $value1) {
                    foreach ($value1 as $key2 => $value2) {
                        $this->vars[$value->id][$type] = array_merge($this->vars[$value->id][$type], explode(',', $value2));
                    }
                }
            }
        }
    }

    private function checkVars(){
        $this->percentes['vars'] = [];
        $counts = [];
        $cc = [];
        $i = 0;
        foreach ($this->vars as $key => $value) {
            if(empty($counts[$key])){
                $counts[$key] = [];
            }
            if(empty($cc[$key])){
                $cc[$key] = [];
            }
            $j = 0;
            foreach ($this->vars as $key1 => $value1) {
                if($key1==$key){
                    $j++;
                    continue;
                }
                if(empty($counts[$key1])){
                    $counts[$key1] = [];
                }
                if(empty($cc[$key1])){
                    $cc[$key1] = [];
                }
                if(empty($cc[$key][$key1])){
                    $cc[$key][$key1] = [0,0];
                }
                if(empty($cc[$key1][$key])){
                    $cc[$key1][$key] = [0,0];
                }
                $count_matches = 0;
                $all_count1 = 0;
                $all_count2 = 0;
                foreach ($this->types as $k => $type) {
                    $all_count1 += count($value[$type]);
                    $all_count2 += count($value1[$type]);
                    if(!empty($value[$type]) && !empty($value1[$type])){
                        foreach ($value[$type] as $key2 => $value2) {
                            if(empty($counts[$key][$value2])){
                                $counts[$key][$value2] = 0;
                                $matches1 = [];
                                preg_match_all('/[+,-,\/,\*,=, ,\t,(,),<,>]('.$value2.')[+,-,\/,\*,=, ,\t,;,),(,>,<]/', $this->codes[$i]->code, $matches1);
                                array_shift($matches1);
                                foreach ($matches1 as $key4 => $value4) {
                                    $counts[$key][$value2] += count($value4);
                                }
                            }
                            foreach ($value1[$type] as $key3 => $value3) {
                                if(empty($counts[$key1][$value3])){
                                    $counts[$key1][$value3] = 0;
                                    $matches2 = [];
                                    preg_match_all('/[+,-,\/,\*,=, ,\t,(,),<,>]('.$value3.')[+,-,\/,\*,=, ,\t,;,),(,>,<]/', $this->codes[$j]->code, $matches2);
                                    array_shift($matches2);
                                    foreach ($matches2 as $key4 => $value4) {
                                        $counts[$key1][$value3] += count($value4);
                                    }
                                }
                                if($value2 == $value3){
                                    $cc[$key][$key1][0] += $counts[$key][$value2];
                                    $cc[$key][$key1][1] += $counts[$key1][$value3];
                                    $cc[$key1][$key][0] += $counts[$key1][$value3];
                                    $cc[$key1][$key][1] += $counts[$key][$value2];
                                    $count_matches+=1;
                                    break;
                                }elseif (stripos($value2, $value3) != false || stripos($value3, $value2) != false){
                                    $cc[$key][$key1][0] += $counts[$key][$value2]/2;
                                    $cc[$key][$key1][1] += $counts[$key1][$value3]/2;
                                    $cc[$key1][$key][0] += $counts[$key1][$value3]/2;
                                    $cc[$key1][$key][1] += $counts[$key][$value2]/2;
                                    $count_matches+=0.5;
                                    break;
                                }
                            }
                        }
                    }
                }
                if($all_count1 == 0){
                    $pers1 = 0;
                }else{
                    $pers1 = 100 / $all_count1 * $count_matches;
                }
                if($cc[$key][$key1][1] == 0){
                    $pers2 = 0;
                }else{
                    $pers2 = 100 / $cc[$key][$key1][1] * $cc[$key][$key1][0];
                }
                if($all_count2 == 0){
                    $pers3 = 0;
                }else{
                    $pers3 = 100 / $all_count2 * $count_matches;
                }
                if($cc[$key1][$key][1] == 0){
                    $pers4 = 0;
                }else{
                    $pers4 = 100 / $cc[$key1][$key][1] * $cc[$key1][$key][0];
                }
                $this->percentes['vars'][$key][$key1] = min($pers1, 100)/2+min($pers2,100)/2;
                $this->percentes['vars'][$key1][$key] = min($pers3, 100)/2+min($pers4,100)/2;
                $j++;
            }
            $i++;
        }
    }

    private function calculatePlagiat(){
        foreach ($this->percentes as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if(empty($this->plagiat[$key1])){
                    $this->plagiat[$key1] = [];
                }
                foreach ($value1 as $key2 => $value2) {
                    if(empty($this->plagiat[$key1][$key2])){
                        $this->plagiat[$key1][$key2] = 0;
                    }
                    $this->plagiat[$key1][$key2] += $value2 / count($this->percentes);
                }
            }
        }
    }

    private function getResult(){
        foreach ($this->plagiat as $key => $value) {
            $this->result[Code::find($key)->name] = [];
            foreach ($value as $key1 => $value1) {
                $this->result[Code::find($key)->name][Code::find($key1)->name] = $value1;
            }
        }
    }

    private function reset(){
        foreach ($this->codes as $key => $value) {
            $value->delete();
        }
    }

    private function getOperators(){
        foreach ($this->codes as $key => $value) {
            foreach ($this->operators as $k => $op) {
                $matches = [];
                preg_match_all('/'.$op.'/', $value->code, $matches);
                foreach ($matches as $key1 => $value1) {
                        $this->oprs[$value->id][$op] = count($value1);
                }
            }
        }
    }

    private function checkOperators(){
        $ops = [];
        $this->percentes['operators'] = [];
        foreach ($this->oprs as $key => $value) {
            if(empty($ops[$key])){
                $ops[$key] = [];
            }
            foreach ($this->oprs as $key1 => $value1) {
                if($key == $key1) continue;
                if(empty($ops[$key1])){
                    $ops[$key1] = [];
                }
                if(empty($ops[$key][$key1])){
                    $ops[$key][$key1] = [0,0];
                }
                if(empty($ops[$key1][$key])){
                    $ops[$key1][$key] = [0,0];
                }
                foreach ($this->operators as $key2 => $op) {
                    $ops[$key][$key1][0] += $value[$op];
                    $ops[$key][$key1][1] += $value1[$op];
                    $ops[$key1][$key][1] += $value[$op];
                    $ops[$key1][$key][0] += $value1[$op];
                }
                $this->percentes['operators'][$key] = [];
                if($ops[$key][$key1][1] == 0){
                    $rep1 = 0;
                }else{
                    $rep1 = 100 / $ops[$key][$key1][1] * $ops[$key][$key1][0];
                }
                if($ops[$key1][$key][1] == 0){
                    $rep2 = 0;
                }else{
                    $rep2 = 100 / $ops[$key1][$key][1] * $ops[$key1][$key][0];
                }
                $this->percentes['operators'][$key][$key1] = min($rep1, 100);
                $this->percentes['operators'][$key1][$key] = min($rep2, 100);
            }   
        }
    }

}