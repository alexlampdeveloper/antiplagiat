<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Code;
use App\Language;

class CodeController extends Controller
{

    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function add(Request $request){
        $this->jwt->setToken($request->input('token'));
        $token =  $this->jwt->getToken();
        $decoded =  $this->jwt->decode($token);
        $token = $decoded->get('sub');
        if(empty($token)){
            return response()->json(['error' => 'Token required'], 400);
        }
        $user = User::getByToken($token);
        if(empty($user)){
            return response()->json(['error' => 'Token incorrect'], 400);
        }
        $code = new Code;
        $code->name = $decoded->get('name');
        if(empty($code->name)){
            return response()->json(['error' => 'Code name required'], 400);
        }
        $code->code = $decoded->get('code');
        if(empty($code->code)){
            return response()->json(['error' => 'Code required'], 400);
        }
        $code->user_id = $user->id;
        $language_name = $decoded->get('language');
        if(empty($language_name)){
            return response()->json(['error' => 'Code language required'], 400);
        }
        $language = Language::getByName($language_name);
        if(empty($language)){
            return response()->json(['error' => 'Language incorrect'], 400);
        }
        $code->language_id = $language->id;
        $code->save();
        return response(null, 200);
    }
}