<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class CodeTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddCode(){
        $dataUsers = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'email' => 'example@gmail.com',
            'password' => uniqid()
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($dataUsers);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/register', ['token' => $token]);
        $login = [
            'email' => $dataUsers['email'],
            'password' => $dataUsers['password'],
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($login);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/login', ['token' => $token]);
        $login['token'] = json_decode($response->content())->token;
        $data = [
            'name' => uniqid(),
            'code' => 'program ideone;
                        var n:integer; ans:real;
                        begin
                            n := 5;
                            ans := 2;
                            while n<=98 do
                            begin
                                ans := sqrt(ans) + n;
                                n+=3;
                            end;
                            write(sqrt(ans):0:2);
                        end.',
            'language' => 'pascal'
        ];
        foreach ($data as $key => $value) {
            $mem = $value;
            $data[$key] = "";
            $payload = JWTFactory::sub($login['token'])->customClaims($data);
            $token = JWTAuth::encode($payload->make())->get();
            $response = $this->call('POST', '/code/add', ['token' => $token]);
            $this->assertEquals(400, $response->status());
            $data[$key] = $mem;
        }
        $payload = JWTFactory::sub($login['token']."ll")->customClaims($data);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/code/add', ['token' => $token]);
        $this->assertEquals(400, $response->status());
        $payload = JWTFactory::sub($login['token'])->customClaims($data);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/code/add', ['token' => $token]);
        $this->assertEquals(200, $response->status());
    }

    public function testPacalPlagiat(){
        $dataUsers = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'email' => 'example@gmail.com',
            'password' => uniqid()
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($dataUsers);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/register', ['token' => $token]);
        $login = [
            'email' => $dataUsers['email'],
            'password' => $dataUsers['password'],
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($login);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/login', ['token' => $token]);
        $login['token'] = json_decode($response->content())->token;
        $codes = [
            [   
                'name' => uniqid(),
                'code' => 'program ideone;
var a:integer; ans:real;
begin
    ans := 2;
    while n<=98 do
    begin
        b := sqrt(b) + a;
        a+=3;
    end;
    write(sqrt(ans):0:2);
end.',
                'language' => 'pascal'
            ],
            [
                'name' => uniqid(),
                'code' => 'program myprogram;
var c:integer; b:real;
begin
    b := 2;
    while c<=98 do
    begin
        b := sqrt(b) + c;
        c+=3;
    end;
    write(sqrt(b):0:2);
end.',
                'language' => 'pascal'
            ],
        ];
        $payload = JWTFactory::sub($login['token'])->customClaims($codes[0]);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/code/add', ['token' => $token]);
        $payload = JWTFactory::sub($login['token'])->customClaims($codes[1]);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/code/add', ['token' => $token]);
        $payload = JWTFactory::sub($login['token'])->customClaims(['language' => 'pascal']);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/plagiat/start', ['token' => $token]);
        dd($response->content());
        $this->assertEquals(200, $response->status());
        $this->assertEquals(0, DB::table('code')->where('name', $codes[0]['name'])->where('name', $codes[1]['name'])->count());
    }
}
