<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegisterUser()
    {
        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'email' => 'example@gmail.com',
            'password' => uniqid()
        ];
        foreach ($data as $key => $value) {
            $mem = $value;
            $data[$key] = "";
            $payload = JWTFactory::sub('1234567890')->customClaims($data);
            $token = JWTAuth::encode($payload->make())->get();
            $response = $this->call('POST', '/auth/register', ['token' => $token]);
            $this->assertEquals(400, $response->status());
            $data[$key] = $mem;
        }
        $payload = JWTFactory::sub('1234567890')->customClaims($data);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/register', ['token' => $token]);
        $this->assertEquals(200, $response->status());
        $this->seeInDatabase('users', [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email']
        ]);
    }

    public function testLogin()
    {
        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'email' => 'example@gmail.com',
            'password' => uniqid()
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($data);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/register', ['token' => $token]);
        $login = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];
        $payload = JWTFactory::sub('1234567890')->customClaims($login);
        $token = JWTAuth::encode($payload->make())->get();
        $response = $this->call('POST', '/auth/login', ['token' => $token]);
        $this->assertEquals('200', $response->status());
        $token = json_decode($response->content())->token;
        $this->seeInDatabase('users', [
            'email' => $data['email'],
            'token' => $token
        ]);
    }
}
